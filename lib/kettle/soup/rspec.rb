# frozen_string_literal: true

require_relative "rspec/version"

module Kettle
  module Soup
    module Rspec
      class Error < StandardError; end
      # Your code goes here...
    end
  end
end
